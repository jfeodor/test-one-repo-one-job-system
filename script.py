import logging
import os
import sys
import typing
from dataclasses import dataclass

import grpc
from grpc_interceptor import ClientCallDetails, ClientInterceptor

import spm_pb2
import spm_pb2_grpc

class Interceptor(ClientInterceptor):
    def __init__(self, access_token: str):
        self._access_token = access_token

    def intercept(self, method: typing.Callable, request_or_iterator: typing.Any, call_details: grpc.ClientCallDetails):
        new_details = ClientCallDetails(
            call_details.method,
            call_details.timeout,
            [("token", self._access_token)],
            call_details.credentials,
            call_details.wait_for_ready,
            call_details.compression,
        )

        return method(request_or_iterator, new_details)


@dataclass
class LaunchContext:
    refresh_token: str
    launch_id: str
    project_id: str
    scenario_id: str
    job_id: str


def grpc_channel(api_host: str, api_port: str, force_insecure: bool):
    options: dict[str, typing.Any] = {}
    if force_insecure:
        return grpc.insecure_channel(f'{api_host}:{api_port}', options)
    else:
        return grpc.secure_channel(f'{api_host}:{api_port}', grpc.ssl_channel_credentials(), options)


def spm_client(channel: grpc.Channel):
    return spm_pb2_grpc.SPMStub(channel)


def token_client(channel: grpc.Channel):
    return spm_pb2_grpc.TokenManagerStub(channel)


def get_access_token(token_client: spm_pb2_grpc.TokenManagerStub, context: LaunchContext):
    access_token_request = spm_pb2.RefreshRequest(
        refresh_token=spm_pb2.Token(val=context.refresh_token),
        execution_id=context.launch_id,
        project_id=context.project_id,
        scenario_id=context.scenario_id,
    )
    access_token = token_client.GetAccessToken(access_token_request)
    return str(access_token.val)


def main(spm_client: spm_pb2_grpc.SPMStub, context: LaunchContext):
    scenario_meta_data = spm_pb2.ScenarioMetaData(
        project=context.project_id,
        scenario=context.scenario_id,
        execution=context.launch_id,
        tags=[spm_pb2.Tag(name='tag1')],
        timezone='UTC',
        epoch_type='s'
    )
    spm_client.SetScenarioMetaData(scenario_meta_data)
    spm_client.SetScenarioProgress(
        request=spm_pb2.ScenarioProgress(
            project=context.project_id,
            scenario=context.scenario_id,
            job_id=context.job_id,
            message='Running...',
            status='running',
            progress=0.0
        )
    )

    list(spm_client.WriteDataStream(
        request_iterator=iter([spm_pb2.WriteDataStreamRequest(
            scenario=context.scenario_id,
            execution=context.launch_id,
            index=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            data={'tag1': spm_pb2.StreamData(values=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])},
            update_stats=True
        )])
    ))

    spm_client.SetScenarioProgress(
        request=spm_pb2.ScenarioProgress(
            project=context.project_id,
            scenario=context.scenario_id,
            job_id=context.job_id,
            message='Done',
            status='finished',
            progress=100.0
        )
    )


if __name__ == '__main__':
    print(os.getenv('HYDRATED_SYSTEM'))
    api_host = os.environ['NUMEROUS_API_HOST']
    api_port = os.environ['NUMEROUS_API_PORT']
    force_insecure = os.getenv('FORCE_INSECURE', 'False') == 'True'
    launch_context = LaunchContext(
        refresh_token=os.environ['NUMEROUS_REFRESH_TOKEN'],
        launch_id=os.environ['NUMEROUS_LAUNCH_ID'],
        project_id=os.environ['NUMEROUS_PROJECT_ID'],
        scenario_id=os.environ['NUMEROUS_SCENARIO_ID'],
        job_id=os.environ['NUMEROUS_JOB_ID']
    )

    channel = grpc_channel(api_host, api_port, force_insecure)
    access_token = get_access_token(token_client(channel), launch_context)
    channel = grpc.intercept_channel(channel, Interceptor(access_token))

    try:
        main(spm_client=spm_client(channel), context=launch_context)
    except:
        logging.exception('Job failed')
        sys.exit(1)
    else:
        sys.exit(0)
