FROM python:3.9
ARG BOOTSTRAP_CONTEXT_DIRECTORY
ARG HYDRATED_SYSTEM

RUN pip install --extra-index-url=https://pypi.numerously.com/simple numerous_api_client==0.16.5

RUN echo ${BOOTSTRAP_CONTEXT_DIRECTORY}
COPY ${BOOTSTRAP_CONTEXT_DIRECTORY}/script.py /script.py
ENV HYDRATED_SYSTEM=HYDRATED_SYSTEM

ENTRYPOINT [ "python", "/script.py" ]
